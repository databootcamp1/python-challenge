# Import the modules for reading csv files
import os
import csv

# Get the current directory
current_directory = os.path.dirname(__file__)

# Specify the subfolder and the CSV file name
csv_folder = "Resources"
csv_file = "budget_data.csv"

# Specify the file path
csvpath = os.path.join(current_directory, csv_folder, csv_file)

# Create arrays to store the data
date = []
profit_losses = []

# Open the CSV file
with open(csvpath) as csvfile:
    csvreader = csv.reader(csvfile, delimiter=",")

# Store the header row
    header = next(csvreader)

# Loop through the data
    for row in csvreader:

# Extract and append values from Date and Profit/Losses to their respective arrays
        date.append(row[0])
        profit_losses.append(int(row[1]))

# Print the results title per requested in assignment
print("Financial Analysis")
print("------------------")

# Calculate total number of months included in the dataset and print
print("Total Months: " + str(len(date)))

# Calculate net total amount of Profit/Losses over the entire period
total_profit_losses = sum(profit_losses)

# Print net total amount of Profit/Losses over the entire period
print("Total: " + str(total_profit_losses))

# Create an array to store changes in Profit/Losses
change_profit_losses = []

# Calculate the changes in Profit/Losses over entire period
for i in range(1, len(profit_losses)):
    current_month_profit = profit_losses[i]
    previous_month_profit = profit_losses[i - 1]
    change = current_month_profit - previous_month_profit
    change_profit_losses.append(change)

# Calculate the average of those changes and print
print("Average Change: " + str(sum(change_profit_losses)/len(change_profit_losses)))

# Calculate the greatest increase in profits (amount) over entire period
greatest_profit_increase = max(change_profit_losses)

# Pull the date of the greatest profit increase amount
profit_change_index = change_profit_losses.index(greatest_profit_increase)
date_change_increase = date[profit_change_index + 1]

# Calculate the greatest decrease in profits (amount) over entire period
greatest_profit_decrease = min(change_profit_losses)

# Pull the date of the greatest profit decrease amount
profit_change_index = change_profit_losses.index(greatest_profit_decrease)
date_change_decrease = date[profit_change_index + 1]

# Print greatest profit increase and decrease
print("Greatest Increase in Profits: " + date_change_increase + (", ") + str(greatest_profit_increase))
print("Greatest Decrease in Profits: " + date_change_decrease + (", " + str(greatest_profit_decrease)))

# Export data to a text file
data_to_export = f"""
"Financial Analysis"
------------------
Total Months: {str(len(date))}
Total: {str(total_profit_losses)}
Average Change: {str(sum(change_profit_losses)/len(change_profit_losses))}
Greatest Increase in Profits: {date_change_increase + (", ") + str(greatest_profit_increase)}
Greatest Decrease in Profits: {date_change_decrease + (", " + str(greatest_profit_decrease))}
"""

# File path for the text file
file_path = "PyBank/Analysis/pybank_output.txt"

# Open the file and write results
with open(file_path, "w") as file:
    file.write(data_to_export)