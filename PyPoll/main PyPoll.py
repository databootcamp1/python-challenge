# Import the modules for reading csv files
import os
import csv

# Get the current directory
current_directory = os.path.dirname(__file__)

# Specify the subfolder and the CSV file name
csv_folder = "Resources"
csv_file = "election_data.csv"

# Specify the file path
csvpath = os.path.join(current_directory, csv_folder, csv_file)

# Create a variable for the text file output at the end
text_file_output = ""

# Create an array to store the data
candidates = []

# Open the CSV file
with open(csvpath) as csvfile:
    csvreader = csv.reader(csvfile, delimiter=",")

# Store the header row
    header = next(csvreader)

# Loop through the data
    for row in csvreader:

# Extract and append values to their respective arrays
        candidates.append(row[2])

# Add the results title per requested in assignment (using the variable for the text file output)
text_file_output = "Election Results\n"
text_file_output += "---------------------\n"

# Calcultate the total number of votes cast (use the variable for the text file output)
text_file_output += "Total Votes: " + str(len(candidates)) + "\n"
text_file_output += "---------------------\n"

# Get a complete list of candidates who received votes
candidate_list = {}

# {candidate_name: vote count}

for candidate in candidates:   
    if candidate in candidate_list:
        candidate_list[candidate] = candidate_list[candidate] + 1
    else:
        candidate_list[candidate] = 1

# Create a variable for the winner's name
winner_name = ""
winner_votes = 0

# Get names, percentage, and total number of votes for each candidate (separately and using the variable for the text file output)
for each_candidate in candidate_list:
    votes = candidate_list[each_candidate]
    percentage = votes/(len(candidates)) * 100
    formatted_percentage = "{:.5}".format(percentage)
    text_file_output += f"{each_candidate}, {formatted_percentage}%, {votes}\n"

#Find winner of the election
    if votes > winner_votes:
         winner_votes = votes
         winner_name = each_candidate

# Add separation requested in assignment
text_file_output += "---------------------\n"

# Find the election winner
text_file_output += "Winner: " + winner_name + "\n"

#Print results
print(text_file_output)

# File path for the text file
file_path = "PyPoll/Analysis/pypoll_output.txt"

# Open the file and write results
with open(file_path, "w") as file:
    file.write(text_file_output)
